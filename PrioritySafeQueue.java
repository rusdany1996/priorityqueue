import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class PrioritySafeQueue<T extends Comparable<T>> {
	
	private int size = 0;
	private ArrayList <T> queue;
	ReentrantLock addLock = new ReentrantLock();
	ReentrantLock removeLock = new ReentrantLock();
	
	public PrioritySafeQueue() {
		queue = new ArrayList<T>(11);
	}
	
	public PrioritySafeQueue(int initialCapacity) {
		queue = new ArrayList<T>(initialCapacity);
	}
	
	public void add(T t) {
		if(t == null) {
			throw new IllegalArgumentException("NULL Argument");
		}
		addLock.lock();
		try{
			queue.add(t);
			bottomUp(size);
			size++;
		} finally {
			addLock.unlock();
		}
	}
	
	public T peek() {
		if(!isEmpty()) {
			return queue.get(0);
		}
		else {
			return null;
		}
	}
	
	public T poll() {
		removeLock.lock();
		try {
			if(isEmpty()) {
				return null;
			}
			size--;
			T removedObject = queue.get(0);
			swap(0, size);
			queue.remove(size);
			heapfy(0);
			return removedObject;
		} finally {
			removeLock.unlock();
		}
	}
	
	private boolean compare(int i, int j) {
		return queue.get(i).compareTo(queue.get(j)) >= 0 ;
	}
	
	private void bottomUp(int i) {
		int parent = (i-1) / 2;
		while( i > 0 && compare(i,parent)) {
			swap(parent,i);
			i = parent;
			parent = (i-1)/2;
		}
	}
	
	private void heapfy(int i) {
		int max;
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		if(left < size && compare(left,i)) {
			max = left;
		}
		else {
			max = i;
		}
		if(right < size && compare(right,max)) {
			max = right;
		}
		if(max != i) {
			swap(i,max);
			heapfy(max);
		}
	}
	
	private void swap(int i, int j) {
		T aux = queue.get(i);
		queue.set(i, queue.get(j));
		queue.set(j, aux);
	}
	
	public void update() {
		for(int i = size / 2 - 1; i >= 0; i--) {
			heapfy(i);
		}
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	public int size() {
		return size;
	}
}
